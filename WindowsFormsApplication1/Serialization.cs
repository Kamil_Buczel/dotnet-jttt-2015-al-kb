﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace JTTT
{
    public class Serialization
    {


        static public void Serialize(BindingList<Find> mlist)
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Create);

            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, mlist);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            //mlist.Clear();
        }

        static public BindingList<Find> Deserialize(BindingList<Find> mlist, ListBox listBox1)
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                mlist = (BindingList<Find>)formatter.Deserialize(fs);
                listBox1.DataSource = mlist;

            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();

            }
            return mlist;
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;


using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;

namespace JTTT
{
    public class SendMail
    {
        static public bool wyslij_maila(string SzukaneSlowo, string adresOdbiorcy)
        {
            string nazwaPliku = SzukaneSlowo;
            SmtpClient smtpClient = new SmtpClient(); //tworzymy klienta smtp
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;

            MailMessage message = new MailMessage();//tworzymy wiadomość
            MailAddress from = new MailAddress("kontotestowe1993@gmail.com", "Voc");//adres nadawcy i nazwa nadawcy

            message.From = from;
            message.To.Add(adresOdbiorcy);//adres odbiorcy
            message.Subject = string.Format("Obrazek z podpisem zawierajacym wyraz \"{0}\"", nazwaPliku);//temat wiadomości
            message.Body = "Witaj"; //treść wiadomości

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(String.Format(@"{0}.png", nazwaPliku));
            message.Attachments.Add(attachment);

            smtpClient.Host = "smtp.gmail.com"; //host serwera
            smtpClient.Credentials = new System.Net.NetworkCredential("kontotestowe1993@gmail.com", "kontokonto");//nazwa nadawcy i hasło
            smtpClient.Send(message);

            return false;
        }


        static public bool wyslij_prognoze(string nazwaPliku, string adresOdbiorcy, string tresc)
        {

            SmtpClient smtpClient = new SmtpClient(); //tworzymy klienta smtp
            smtpClient.UseDefaultCredentials = false;
            smtpClient.EnableSsl = true;
            smtpClient.Port = 587;

            MailMessage message = new MailMessage();//tworzymy wiadomość
            MailAddress from = new MailAddress("kontotestowe1993@gmail.com", "Voc");//adres nadawcy i nazwa nadawcy

            message.From = from;
            message.To.Add(adresOdbiorcy);//adres odbiorcy
            message.Subject = string.Format("Dzisiejsza pogoda");//temat wiadomości
            message.Body = tresc; //treść wiadomości

            WebClient ww = new WebClient();
            ww.UseDefaultCredentials = true;

            var stream = ww.OpenRead(nazwaPliku);
            Attachment attachement = new Attachment(stream, "Ikona_prognozy.png");
            message.Attachments.Add(attachement);

            smtpClient.Host = "smtp.gmail.com"; //host serwera
            smtpClient.Credentials = new System.Net.NetworkCredential("kontotestowe1993@gmail.com", "kontokonto");//nazwa nadawcy i hasło
            smtpClient.Send(message);

            return false;
        }


    }
}

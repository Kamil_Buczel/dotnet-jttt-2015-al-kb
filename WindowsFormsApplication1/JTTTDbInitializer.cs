﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{

    public class JTTTDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<JTTTDbContext>
    {

        /// <summary>
        /// Specjalna metoda, która jest wywoływana raz po przebudowaniu bazy danych.
        /// Założenie jest, że baza jest pusta, więc trzeba ją wypełnić początkowymi danymi.
        /// </summary>
        /// <param name="context"></param>
        /// 
        protected override void Seed(JTTTDbContext context)
        {
            Group group;

            group = new Group() { Name = "Example" };



            //group.Items.Add(new Item() { NazwaZadania = "ExampleTask1", Adres = "http://demotywatory.pl/", AdresOdbiorcy = "kontotestowe1993@gmail.com", SzukaneSlowo = "polska" });
            //group.Items.Add(new Item() { NazwaZadania = "ExampleTask2", Adres = "http://demotywatory.pl/", AdresOdbiorcy = "kontotestowe1993@gmail.com", SzukaneSlowo = "polityk" });
            group.Items.Add(new Item() { NazwaZadania = "ExampleTask2", Adres = "http://demotywatory.pl/", AdresOdbiorcy = "kontotestowe1993@gmail.com", SzukaneSlowo = "polityk", Okno = true});

            context.Group.Add(group);


            context.SaveChanges();
            base.Seed(context);
        }
    }
}

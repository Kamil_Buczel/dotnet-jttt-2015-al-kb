﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class JTTTDbContext : DbContext
    {
        // Aby podac wlasna nazwe bazy danych, nalezy wywolac konstruktor bazowy z nazwą jako parametrem.
        public JTTTDbContext()
            : base("JtttBaza6")
        {
            // Użyj klasy StudiaDbInitializer do zainicjalizowania bazy danych.
            Database.SetInitializer<JTTTDbContext>(new JTTTDbInitializer());
        }

        public DbSet<Item> Item { get; set; }

        public DbSet<Group> Group { get; set; }
    }
}

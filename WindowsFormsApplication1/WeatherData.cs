﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace JTTT
{

    public class WeatherData
    {
        public double message { get; set; }
        public string cod { get; set; }
        public string calctime { get; set; }
        public double cnt { get; set; }
        public List<ApiList> list { get; set; }
        public Main main { get; set; }
        public List<Weather> weather { get; set; }
 
    }

    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Weather
    {
        public double id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public double humidity { get; set; }
        public double pressure { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double gust { get; set; }
        public double deg { get; set; }
    }

    public class Clouds
    {
        public double all { get; set; }
    }

    public class ApiList
    {
        public double id { get; set; }
        public string name { get; set; }
        public Coord coord { get; set; }
        public double distance { get; set; }
        public Main main { get; set; }
        public double dt { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public List<Weather> weather { get; set; }
    }
}

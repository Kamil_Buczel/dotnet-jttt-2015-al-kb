namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Try1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NazwaZadania = c.String(),
                        Adres = c.String(),
                        SzukaneSlowo = c.String(),
                        adresOdbiorcy = c.String(),
                        TimeOfMaking = c.DateTime(nullable: false),
                        GrupaId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Groups", t => t.GrupaId)
                .Index(t => t.GrupaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "GrupaId", "dbo.Groups");
            DropIndex("dbo.Items", new[] { "GrupaId" });
            DropTable("dbo.Items");
            DropTable("dbo.Groups");
        }
    }
}

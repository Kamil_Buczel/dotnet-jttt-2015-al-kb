namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class obrazki : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "Okno", c => c.Boolean(nullable: false));
            AddColumn("dbo.Items", "Miasto", c => c.String());
            AddColumn("dbo.Items", "Tt", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "Tt");
            DropColumn("dbo.Items", "Miasto");
            DropColumn("dbo.Items", "Okno");
        }
    }
}

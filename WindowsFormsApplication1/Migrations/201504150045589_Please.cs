namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Please : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Items", "TimeOfMaking");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "TimeOfMaking", c => c.DateTime(nullable: false));
        }
    }
}

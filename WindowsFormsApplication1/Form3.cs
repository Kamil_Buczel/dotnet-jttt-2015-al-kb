﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Form3 : Form
    {
        public Form3(bool pogoda)
        {
            InitializeComponent();
            if(pogoda)
            {
                Text = "Pogoda na najbliższy czas";
                Width = 300;
                Height = 300;
                textBox1.Width = 270;
                textBox1.Height = 30;
                label2.Text = "Ikona stanu pogody";
            }
            else
            {
                Text = "Obrazek znaleziony w sieci";
                label2.Text = "Obrazek";
                Width = 650;
                Height = 750;
                AutoScroll = true;
            }
        }

        public void zmiana(string zmien, string plik)
        {
            textBox1.Text = zmien;
            pictureBox1.ImageLocation = plik;
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}

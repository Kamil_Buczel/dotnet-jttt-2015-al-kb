﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    [Serializable]
    public partial class Form1 : Form
    {
        public BindingList<Find> mlist = new BindingList<Find>();
        int group_id = 0;

      
        public Form1()
        {
            InitializeComponent();
            listBox1.DataSource = mlist;
            domena.Text = "http://demotywatory.pl/";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*numericUpDown.numericUpDownItemCollection items = numericUpDown1.Items;

            for (int i = 35; i > -25; i--)
                items.Add(i);

            numericUpDown1.Text = "15"; */
        }


        private void start_Click(object sender, EventArgs e)
        {
            //Find one = mlist.ElementAt(listBox1.SelectedIndex);




            for (int i = 0; i < mlist.Count(); i++)
            {
                Find one = mlist.ElementAt(i);

                //bool jakie_zad = one.SzukaneSlowo != null;


                if(one.SzukaneSlowo==null) // szukanie pogody
                {
                    String baseUrl = "http://openweathermap.org/img/w/{0}.png";
                    double t = 0;
                    String icon;
                    string miasto = city.Text;
                    WeatherData N = WeatherApiClient.ZwrocObiekt(miasto, "PL");

                    icon = N.weather.First().icon;
                    string iconURL = string.Format(baseUrl, icon);
                    t = N.main.temp;
                    t = t - 273.15;
                    t = Math.Round(t);
                    string wiadomosc = "Aktualna temperatura to: " + t.ToString() + " stopni Celsjusza";

                    if (t >= one.tt)
                    {
                        if (one.okno) // czyli ma wyswietlac okno
                        {
                            Form3 frm = new Form3(true);
                            frm.zmiana(wiadomosc, iconURL);
                            frm.Show();
                        }
                        else // czyli ma wysłać maila
                        {
                            SendMail.wyslij_prognoze(iconURL, one.adresOdbiorcy, wiadomosc);
                        }
                    }

                }
                else // szukanie demotów
                {
                    if (one.okno) // czyli ma wyswietlac okno
                    {
                        if (one.CzyZnaleziono(true))
                        {
                            label4.Text = "Znaleziono hasło";
                        }
                        else
                            label4.Text = "Nie znaleziono hasła";
                    }
                    else // czyli ma wysłać maila
                    {
                        if (one.CzyZnaleziono(false))
                        {
                            label4.Text = "Znaleziono hasło";
                            SendMail.wyslij_maila(one.SzukaneSlowo, one.adresOdbiorcy); //, yourImage.ToString()
                        }
                        else
                            label4.Text = "Nie znaleziono hasła";
                    }
                }


            }
        }

        private void dodaj_Click(object sender, EventArgs e)
        {
            
            var list = new List();
            bool x = tabControl2.SelectedIndex == 1; // dla okienka = 1, dla maila 0
            bool czy_pogoda = tabControl1.SelectedIndex == 1; // dla miasta = 1, dla demotów 0
            int ttemp = (int)numericUpDown1.Value;
            Find temp;
            string edres = adres.Text;

            if (x)
                edres = null;


            if(czy_pogoda)
            {
                temp = new Find(city.Text, ttemp, edres, nazwazadania.Text, x);
            }
            else
            {
                temp = new Find(domena.Text, wyraz.Text, edres, nazwazadania.Text, x);
            }
            
            mlist.Add(temp);
            
            //DO POZNIEJSZEJ REFAKTORYZACJI NA MILOSC BOGA
            if (Int32.TryParse(List_number.Text, out group_id))
            {
                if (czy_pogoda)
                {
                    list.AddItem(group_id, nazwazadania.Text, domena.Text, adres.Text, null, x, city.Text, ttemp);
                }
                else
                {
                    list.AddItem(group_id, nazwazadania.Text, domena.Text, adres.Text, wyraz.Text, x, city.Text, ttemp);
                }
                
            }

            

          
            

        }

        private void usun_Click(object sender, EventArgs e)
        {
            int a = listBox1.SelectedIndex;
            mlist.RemoveAt(a);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (listBox1.Items.Count != 0) Serialization.Serialize(mlist);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            mlist = Serialization.Deserialize(mlist, listBox1);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            mlist.Clear();
           
        }

        private void button7_Click(object sender, EventArgs e)
        {


            if (Int32.TryParse (List_number.Text, out group_id))
            {
                mlist.Clear();
                var list = new List();
                mlist = list.GetItem(group_id);
                listBox1.DataSource = mlist;
            }
            
            


        }

        private void start_MouseHover(object sender, EventArgs e)
        {
            start.ForeColor = Color.Green;
        }

        private void start_MouseLeave(object sender, EventArgs e)
        {
            start.ForeColor = Color.Black;
        }


        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //to po prostu ma być, nie kasować tego
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

    }
}

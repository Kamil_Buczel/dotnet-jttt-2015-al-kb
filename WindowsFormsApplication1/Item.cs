﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Item
    {
        // EF w ramach konwencji zaklada, ze klucz glowny ma nazwe Id.
        public int Id { get; set; }
        public string NazwaZadania { get; set; }
        public string Adres { get; set; }
        public string SzukaneSlowo { get; set; }
        public string AdresOdbiorcy { get; set; }
        public bool Okno { get; set; }
        public string Miasto { get; set; }
        public int Tt { get; set; }
        
        //public DateTime TimeOfMaking { get; set; }

        //public Item(string nazwaZadania, string Adres, string adresOdbiorcy, string SzukaneSlowo)
        //{
        //    this.NazwaZadania = nazwaZadania;
        //    this.Adres = Adres;
        //    this.adresOdbiorcy = adresOdbiorcy;
        //    this.SzukaneSlowo = SzukaneSlowo;
        //}

        // Zmienna GrupaId jest nullable, to znaczy, że moze byc ustawiona jako null.
        // Dzieki temu nie kazdy student musi byc przypisany do grupy.
        // EF w ramach konwencji zaklada, ze klucz obcy do tabeli zewnetrznej to nazwa tabli zakonczona Id.
        public int? GrupaId { get; set; }

        public virtual Group Grupa { get; set; }

        public override string ToString()
        {
            return string.Format("St Id={0}, Nazwa Zadania={1}, Adres Witryny={2}, Adres Odbiorcy={3}, Szukane Slowo={4}, Okno{5}, Miasto={6}, Temperatura ={7}",
                                  Id, NazwaZadania, Adres, AdresOdbiorcy, SzukaneSlowo, Grupa == null ? "null" : Grupa.Name, Okno, Miasto, Tt);
        }
    }
}

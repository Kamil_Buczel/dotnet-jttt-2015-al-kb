﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;



namespace JTTT
{
    [Serializable]
    public class Find
    {
        //nowa linia komentarza
        LogFile log = new LogFile();
        string y = "Sukces!";
        string n = "Porazka :(";
        string adres;
        public string SzukaneSlowo { private set; get; } //publiczny odczyt, private zapis
        public string adresOdbiorcy { private set; get; }
        string NazwaZadania;
        public bool okno { private set; get; } // jezeli 1, to ma byc okienko, jezeli 0, to ma wysylac maila
        public int tt { private set; get; } // temperatura
        string miasto;

        public Find(string adres, string SzukaneSlowo, string adresOdbiorcy, string NazwaZadania, bool okno)
        {
            this.adres = adres;
            this.SzukaneSlowo = SzukaneSlowo;
            this.adresOdbiorcy = adresOdbiorcy;
            this.NazwaZadania = NazwaZadania;
            this.okno = okno;
        }

        public Find(string miasto, int tt, string adresOdbiorcy, string NazwaZadania, bool okno)
        {
            this.miasto = miasto;
            this.tt = tt;
            this.adresOdbiorcy = adresOdbiorcy;
            this.NazwaZadania = NazwaZadania;
            this.okno = okno;
        }
        //s.NazwaZadania, s.Adres, s.AdresOdbiorcy, s.SzukaneSlowo, s.Okno, s.Miasto, s.Tt
        public Find(string NazwaZadania, string adres, string adresOdbiorcy, string SzukaneSlowo, bool okno, string miasto, int tt)
        {
            this.adres = adres;
            this.SzukaneSlowo = SzukaneSlowo;
            this.adresOdbiorcy = adresOdbiorcy;
            this.NazwaZadania = NazwaZadania;
            this.okno = okno;
            this.miasto = miasto;
            this.tt = tt;
        }

        public override string ToString()
        {
            string op = "";
            if (okno)
                op = "W oknie ";

            if(SzukaneSlowo != null)
                return op + NazwaZadania + "     (" + adres + "," + SzukaneSlowo + "," + adresOdbiorcy + ")";
            else
                return op + NazwaZadania + "     (" + miasto + "," + tt + "," + adresOdbiorcy + ")";

        }

        public bool CzyZnaleziono(bool okienko)
        {

            string pageContent = null;
            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(adres);
            HttpWebResponse myres = (HttpWebResponse)myReq.GetResponse();

            using (StreamReader sr = new StreamReader(myres.GetResponseStream()))
            {
                pageContent = sr.ReadToEnd();
            }

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(pageContent);

            log.MyLogFile(y, string.Format("Wczytano zawartość strony \"{0}\".", adres));

            var nodes = doc.DocumentNode.Descendants("img");
            
            foreach (var node in nodes)
            {
                string v = node.Attributes["alt"].Value;
                if (node.Attributes["alt"].Value.Contains(SzukaneSlowo) && node.Attributes["src"].Value.Contains("http://"))
                {
                    using (WebClient webClient = new WebClient())
                    {
                        byte[] data = webClient.DownloadData(node.GetAttributeValue("src", ""));

                        using (MemoryStream mem = new MemoryStream(data))
                        {
                                
                            var yourImage = Image.FromStream(mem);
                             
                            if (!File.Exists((String.Format(@"{0}.png", SzukaneSlowo))))
                            yourImage.Save(String.Format(@"{0}.png", SzukaneSlowo), ImageFormat.Png);

                            log.MyLogFile(y, string.Format("Znaleziono hasło \"{0}\" i pobrano obraz.", SzukaneSlowo));

                            System.Console.WriteLine(yourImage.ToString());
                            if(okienko)
                            {
                                Form3 f1 = new Form3(false);
                                f1.zmiana(System.Net.WebUtility.HtmlDecode(node.Attributes["alt"].Value), node.GetAttributeValue("src", ""));
                                f1.Text = "Obrazek w szukanej witrynie";
                                f1.Show();
                            }
                            
                        }
                    }
                   
                    return true;
                }

            }
            log.MyLogFile(n, string.Format("Nie znaleziono słowa.", SzukaneSlowo));
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT
{
    public class List
    {
        static public BindingList<Find> mlist = new BindingList<Find>();
        /// <summary>
        /// Prosta metoda, ktora wypisuje wszystkie grupy i studentow z bazy danych.
        /// Normalnie takie metody powinny się znaleźć w odpowiednich klasach do obsługi danych
        /// </summary>

        public BindingList<Find> GetItem(int id)
        {
            Group group = null; ;
            // Do odwoływania się do bazy danych używamy obiektu kontekstu
            using (var ctx = new JTTTDbContext())
            {

                foreach (var g in ctx.Group)
                {
                    if (g.Id == id)
                        group = g;

                }
                //Console.WriteLine("----------------");
                //Console.WriteLine(g);
                foreach (var s in group.Items)
                {
                    Find temp;
                   // if (s.Miasto == null)
                        temp = new Find(s.NazwaZadania, s.Adres, s.AdresOdbiorcy, s.SzukaneSlowo, s.Okno, s.Miasto, s.Tt);
                  
                    //s.Adres, s.SzukaneSlowo, s.AdresOdbiorcy, s.NazwaZadania, s.Okno
                    mlist.Add(temp);
                    //Console.WriteLine(s);
                    //Console.WriteLine("Hej kurcze!!!" + s.Name);
                }

                return mlist;
            }
        }


        public void WypiszStudentow()
        {
            // Do odwoływania się do bazy danych używamy obiektu kontekstu
            using (var ctx = new JTTTDbContext())
            {
                // EF domyślnie wykorzystuje lazy loading - jeżeli coś nie jest potrzebne, to nie jest ładowane
                // Aby EF automatycznie moglo zaladowac relacje, musza one byc zdefiniowane jako virtual
                // W przeciwnym wypadku musielibysmy jawnie zadeklarowac powiazanie przez dolaczenie do zapytania tablicy Students, np.: 
                // var grupy = ctx.Grupa.Include("Students");

                foreach (var g in ctx.Group)
                {
                    Console.WriteLine("----------------");
                    Console.WriteLine(g);
                    foreach (var s in g.Items)
                    {
                        Console.WriteLine(s);

                    }
                }
            }
        }

        public void AddItem(int id, string nazwaZadania, string Adres, string adresOdbiorcy, string SzukaneSlowo, bool Okno, string Miasto, int Tt)
        {
            Group group = null; ;
            using (var ctx = new JTTTDbContext())
            {

                    foreach (var g in ctx.Group)
                    {
                        if (g.Id == id)
                            group = g;

                    }


                // Tworzymy grupę w pamięci
                //Group group = new Group() { Name = "Aniołki Charliego" };

                Item item;
                
                //mozliwy refactor na kodzie
                item = new Item() { NazwaZadania = nazwaZadania, Adres = Adres, AdresOdbiorcy = adresOdbiorcy, SzukaneSlowo = SzukaneSlowo, Okno = Okno, Miasto = Miasto, Tt = Tt };
                group.Items.Add(item);

                // Dodajemy kolejnego studenta do grupy

                // Grupę dodajemy do kolekcji reprezentującej dane w bazie danych
                //ctx.Group.Add(group);

                // Zapisujemy zmienne przechowywane w kontekście
                ctx.SaveChanges();
            }
        }

    }



    }

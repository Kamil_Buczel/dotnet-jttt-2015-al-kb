﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Group
    {
        public Group()
        {
            Items = new List<Item>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        // Referencja do Grupy jest virtual. Dzieki temu EF bedzie mogl wykorzystywac lazy loading
        // do automatycznego zaladowania powiazanych danych.
        public virtual List<Item> Items { get; set; }

        public override string ToString()
        {
            return string.Format("Grupa Id={0}, Name={1}, Licznosc={2}", Id, Name, Items == null ? 0 : Items.Count);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace JTTT
{
    public static class WeatherApiClient
    {
        #region fields

        /// <summary>
        /// Base URL for the Weather Endpoint URL
        /// </summary>
        private const string baseUrl = "http://api.openweathermap.org/data/2.5/weather?q={0},{1}";
        ///// http://api.openweathermap.org/data/2.5/weather?q=Wroclaw,pl

        #endregion

        #region methods

        public static double GetWeatherForecast(string city, string country)
        {
            // Customize URL according to geo location parameters
            var url = string.Format(baseUrl, city, country);

            // Syncronious Consumption
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(url);

            // Create the Json serializer and parse the response
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(WeatherData));
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
            {
                // deserialize the JSON object using the WeatherData type.
                var weatherData = (WeatherData)serializer.ReadObject(ms);
                return weatherData.main.temp;
            }
        }

        public static WeatherData ZwrocObiekt(string city, string country)
        {
            // Customize URL according to geo location parameters
            var url = string.Format(baseUrl, city, country);

            // Syncronious Consumption
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(url);

            // Create the Json serializer and parse the response
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(WeatherData));
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(content)))
            {
                // deserialize the JSON object using the WeatherData type.
                var weatherData = (WeatherData)serializer.ReadObject(ms);
                return weatherData;
            }
        }



        #endregion
    }
}
